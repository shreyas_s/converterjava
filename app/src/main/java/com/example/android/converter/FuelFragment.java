package com.example.android.converter;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * A simple {@link Fragment} subclass.
 */
public class FuelFragment extends Fragment {
    Spinner text_meter, spinner2;
    Button button_meter;
    EditText mEdit;
    TextView result1, result2, result3, result4, result5,result6,result7, result8;

    JsonParser jp = new JsonParser();
    String converstionsString = "{\"Kilometer/litre\":\"1\", \"Kilometer/gallon(US)\":\"3.78541\", \"Kilometer/gallon(UK)\":\"4.54609\", \"Miles/litre\":\"0.62137\", \"Miles/gallon(US)\":\"2.35214\", \"Miles/gallon(UK)\":\"2.8248\", \"Litre/100Km\":\"100\", \"litre/100miles\":\"160.9344\"}";
    JsonObject conversionObject = (JsonObject) jp.parse( converstionsString );


    public FuelFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View root =  inflater.inflate(R.layout.fragment_fuel, container, false);
        text_meter = (Spinner) root.findViewById(R.id.spinner2);
        button_meter = (Button)root.findViewById(R.id.button);
        mEdit = (EditText)root.findViewById(R.id.Text_value);
        result1 = (TextView) root.findViewById(R.id.Text_value1);
        result2 = (TextView) root.findViewById(R.id.Text_value2);
        result3 = (TextView) root.findViewById(R.id.Text_value3);
        result4 = (TextView) root.findViewById(R.id.Text_value4);
        result5 = (TextView)root.findViewById(R.id.Text_value5);
        result6 = (TextView)root.findViewById(R.id.Text_value6);
        result7 = (TextView)root.findViewById(R.id.Text_value7);
        result8 = (TextView)root.findViewById(R.id.Text_value8);
        spinner2 = (Spinner) root.findViewById(R.id.spinner2);


        /*This is Font area */
        Typeface Lato = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Light.ttf");
        Typeface LatoMedium = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Medium.ttf");
        Typeface LatoRegular = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Regular.ttf");


        TextView tv1 = (TextView)root.findViewById(R.id.textView1);
        TextView tv2 = (TextView)root.findViewById(R.id.textView2);
        TextView tv3 = (TextView)root.findViewById(R.id.textView3);
        TextView tv4 = (TextView)root.findViewById(R.id.textView4);
        TextView tv5 = (TextView)root.findViewById(R.id.textView5);
        TextView tv6 = (TextView)root.findViewById(R.id.textView6);
        TextView tv7 = (TextView)root.findViewById(R.id.textView7);
        TextView tv8 = (TextView)root.findViewById(R.id.textView8);
        TextView tv9 = (TextView)root.findViewById(R.id.Text_value1);
        TextView tv10 = (TextView)root.findViewById(R.id.Text_value2);
        TextView tv11 = (TextView)root.findViewById(R.id.Text_value3);
        TextView tv12 = (TextView)root.findViewById(R.id.Text_value4);
        TextView tv13 = (TextView)root.findViewById(R.id.Text_value5);
        TextView tv14 = (TextView)root.findViewById(R.id.Text_value6);
        TextView tv15 = (TextView)root.findViewById(R.id.Text_value7);
        TextView tv16 = (TextView)root.findViewById(R.id.Text_value8);
        TextView tv17 = (TextView)root.findViewById(R.id.title);
        Button bt18 =(Button)root.findViewById(R.id.button);

        tv1.setTypeface(Lato);
        tv2.setTypeface(Lato);
        tv3.setTypeface(Lato);
        tv4.setTypeface(Lato);
        tv5.setTypeface(Lato);
        tv6.setTypeface(Lato);
        tv7.setTypeface(Lato);
        tv8.setTypeface(Lato);
        tv9.setTypeface(Lato);
        tv10.setTypeface(Lato);
        tv11.setTypeface(Lato);
        tv12.setTypeface(Lato);
        tv13.setTypeface(Lato);
        tv14.setTypeface(Lato);
        tv15.setTypeface(Lato);
        tv16.setTypeface(Lato);
        tv17.setTypeface(LatoMedium);
        bt18.setTypeface(LatoRegular);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.Fuel, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);

        button_meter.setOnClickListener(
                new View.OnClickListener() {

                    public void onClick(View view) {

                        Float divisor = Float.valueOf(conversionObject.get(spinner2.getSelectedItem().toString()).getAsString());

                        Float val = Float.valueOf(mEdit.getText().toString()) / divisor;

                        result1.setText(String.valueOf(val * 1));
                        result2.setText(String.valueOf(val * 3.78541));
                        result3.setText(String.valueOf(val * 4.54609));
                        result4.setText(String.valueOf(val * 0.62137));
                        result5.setText(String.valueOf(val * 2.35214));
                        result6.setText(String.valueOf(val * 2.8248));
                        result7.setText(String.valueOf(val * 100));
                        result8.setText(String.valueOf(val * 160.9344));


                    }
                });


    return root;
    }


}
