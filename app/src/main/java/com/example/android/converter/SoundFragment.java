package com.example.android.converter;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * A simple {@link Fragment} subclass.
 */
public class SoundFragment extends Fragment {
    Spinner text_meter, spinner2;
    Button button_meter;
    EditText mEdit;
    TextView result1, result2, result3, result4;
    JsonParser jp = new JsonParser();
    String converstionsString = "{\"Bel(B)\":\"1\", \"Decibel(dB)\":\"10\", \"Neper(Np)\":\"1.1512779185\"}";
    JsonObject conversionObject = (JsonObject) jp.parse( converstionsString );


    public SoundFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View root = inflater.inflate(R.layout.fragment_sound, container, false);
        text_meter = (Spinner) root.findViewById(R.id.spinner2);
        button_meter = (Button)root.findViewById(R.id.button);
        mEdit = (EditText)root.findViewById(R.id.Text_value);
        result1 = (TextView) root.findViewById(R.id.Text_value1);
        result2 = (TextView) root.findViewById(R.id.Text_value2);
        result3 = (TextView) root.findViewById(R.id.Text_value3);

        spinner2 = (Spinner) root.findViewById(R.id.spinner2);

        /*Fonts */
        Typeface Lato = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Light.ttf");
        Typeface LatoMedium = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Medium.ttf");
        Typeface LatoRegular = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Regular.ttf");

        TextView tv1 = (TextView)root.findViewById(R.id.textView1);
        TextView tv2 = (TextView)root.findViewById(R.id.textView2);
        TextView tv3 = (TextView)root.findViewById(R.id.textView3);

        TextView tv21 = (TextView)root.findViewById(R.id.Text_value1);
        TextView tv22 = (TextView)root.findViewById(R.id.Text_value2);
        TextView tv23 = (TextView)root.findViewById(R.id.Text_value3);

        tv1.setTypeface(Lato);
        tv2.setTypeface(Lato);
        tv3.setTypeface(Lato);

        tv21.setTypeface(Lato);
        tv22.setTypeface(Lato);
        tv23.setTypeface(Lato);

        TextView tv5 = (TextView)root.findViewById(R.id.title);
        Button bt1 =(Button)root.findViewById(R.id.button);
        tv5.setTypeface(LatoMedium);
        bt1.setTypeface(LatoRegular);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.Sound, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);
        button_meter.setOnClickListener(
                new View.OnClickListener() {

                    public void onClick(View view) {

                        Float divisor = Float.valueOf(conversionObject.get(spinner2.getSelectedItem().toString()).getAsString());

                        Float val = Float.valueOf(mEdit.getText().toString()) / divisor;

                        result1.setText(String.valueOf(val * 1));
                        result2.setText(String.valueOf(val * 10));
                        result3.setText(String.valueOf(val * 1.1512779185));

                    }
                });



    return root;
    }


}
