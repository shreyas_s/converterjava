package com.example.android.converter;

import com.google.gson.JsonObject;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

/**
 * Created by shreyas on 6/7/15.
 */
public class MyRestClient {

    private static RestInterface restInterface;

    public static RestInterface getClient(){
        if( restInterface == null ) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("http://apilayer.net/api")
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            restInterface = restAdapter.create(RestInterface.class);
        }
        return restInterface;
    }

    public interface RestInterface {

        @GET("/live?access_key=7c1ae65f4e0d77ead7b16c9bb4bd0f90&currencies=EUR,GBP,CAD,PLN,USD,INR,MXN,JPN,AUD,BGN,CZK,CNY,DKK&source=USD&format=1")
        void getCurrencies(Callback<JsonObject> callback);

    }
}
