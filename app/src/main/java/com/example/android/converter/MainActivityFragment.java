package com.example.android.converter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;




/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements View.OnClickListener {

    ImageButton imagebutton_currency;
    ImageButton imagebutton_meter;
    ImageButton imageButton_acceleration;
    ImageButton imageButton_angle;
    ImageButton imageButton_area;
    ImageButton imageButton_energy;
    ImageButton imagebutton_flowrate;
    ImageButton imageButton_force;
    ImageButton imageButton_fuel;
    ImageButton imageButton_magnetic;
    ImageButton imageButton_power;
    ImageButton imageButton_pressure;
    ImageButton imageButton_sound;
    ImageButton imageButton_speed;
    ImageButton imageButton_storage;
    ImageButton imageButton_Temperature;
    ImageButton imageButton_time;
    ImageButton imageButton_torque;
    ImageButton imageButton_viscosity;
    ImageButton imageButton_velocity;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        imagebutton_currency = (ImageButton) view.findViewById(R.id.imagebutton_currency);
        imagebutton_currency.setOnClickListener(this);
        imagebutton_meter = (ImageButton) view.findViewById(R.id.imagebutton_meter);
        imagebutton_meter.setOnClickListener(this);
        imageButton_acceleration = (ImageButton)view.findViewById(R.id.imagebutton_acceleration);
        imageButton_acceleration.setOnClickListener(this);
        imageButton_angle = (ImageButton)view.findViewById(R.id.imagebutton_angle);
        imageButton_angle.setOnClickListener(this);
        imageButton_area = (ImageButton)view.findViewById(R.id.imagebutton_area);
        imageButton_area.setOnClickListener(this);
        imageButton_energy = (ImageButton)view.findViewById(R.id.imagebutton_energy);
        imageButton_energy.setOnClickListener(this);
        imagebutton_flowrate = (ImageButton)view.findViewById(R.id.imagebutton_flowrate);
        imagebutton_flowrate.setOnClickListener(this);
        imageButton_force = (ImageButton)view.findViewById(R.id.imagebutton_force);
        imageButton_force.setOnClickListener(this);
        imageButton_fuel = (ImageButton)view.findViewById(R.id.imagebutton_fuel);
        imageButton_fuel.setOnClickListener(this);
        imageButton_magnetic =(ImageButton)view.findViewById(R.id.imagebutton_magnetic);
        imageButton_magnetic.setOnClickListener(this);
        imageButton_power = (ImageButton)view.findViewById(R.id.imagebutton_power);
        imageButton_power.setOnClickListener(this);
        imageButton_pressure =(ImageButton)view.findViewById(R.id.imagebutton_pressure);
        imageButton_pressure.setOnClickListener(this);
        imageButton_sound =(ImageButton)view.findViewById(R.id.imagebutton_sound);
        imageButton_sound.setOnClickListener(this);
        imageButton_speed = (ImageButton)view.findViewById(R.id.imagebutton_speed);
        imageButton_speed.setOnClickListener(this);
        imageButton_storage = (ImageButton)view.findViewById(R.id.imagebutton_storage);
        imageButton_storage.setOnClickListener(this);
        imageButton_Temperature = (ImageButton)view.findViewById(R.id.imagebutton_temp);
        imageButton_Temperature.setOnClickListener(this);
        imageButton_time = (ImageButton)view.findViewById(R.id.imagebutton_time);
        imageButton_time.setOnClickListener(this);
        imageButton_torque = (ImageButton)view.findViewById(R.id.imagebutton_torque);
        imageButton_torque.setOnClickListener(this);
        imageButton_viscosity = (ImageButton)view.findViewById(R.id.imagebutton_viscosity);
        imageButton_viscosity.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        FragmentManager fm = getFragmentManager();
        Fragment frag = new Fragment();
        switch ( v.getTag().toString() ) {
            case "Currency":
                frag = new CurrencyFragment();
                break;
            case "Meter":
                frag = new MeterFragment();
                break;
            case "Acceleration":
                frag = new AccelerationFragment();
                break;
            case "Angle":
                frag = new AngleFragment();
                break;
            case "Area":
                frag = new AreaFragment();
                break;
            case "Energy":
                frag = new EnergyFragment();
                break;
            case "Flowrate":
                frag = new FlowRateFragment();
                break;
            case "Force":
                frag = new ForceFragment();
                break;
            case "Fuel":
                frag = new FuelFragment();
                break;
            case "Magnetic":
                frag= new MagneticFragment();
                break;
            case "Power":
                frag = new PowerFragment();
                break;
            case "Pressure":
                frag = new PressureFragment();
                break;
            case "Sound":
                frag = new SoundFragment();
                break;
            case"Speed":
                frag= new SpeedFragment();
                break;
            case "Storage":
                frag = new StorageFragment();
                break;
            case "Temperature":
                frag = new TempratureFragment();
                break;
            case"Time":
                frag = new TimeFragment();
                break;
            case "Torque":
                frag = new TorqueFragment();
                break;
            case "Viscosity":
                frag = new ViscosityFragment();
                break;

        }
        fm.beginTransaction().replace( R.id.fragment, frag ).addToBackStack(null).commit();
    }
}
