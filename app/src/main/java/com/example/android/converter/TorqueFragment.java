package com.example.android.converter;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * A simple {@link Fragment} subclass.
 */
public class TorqueFragment extends Fragment {
    Spinner text_meter, spinner2;
    Button button_meter;
    EditText mEdit;
    TextView result1, result2, result3, result4, result5,result6,result7;

    JsonParser jp = new JsonParser();
    String converstionsString = "{\"Newton-meter\":\"1\", \"Newtoncenti-meter\":\"100\", \"Newtonmilli-meter\":\"1000\", \"Kilonewton-meter\":\"0.001\", \"Kilogramforce-meter\":\"0.001\", \"Dyne-meter\":\"100000\", \"poundforce-foot\":\"0.7375621492\", \"poundforce-inch\":\"8.8507457898\"}";
    JsonObject conversionObject = (JsonObject) jp.parse( converstionsString );


    public TorqueFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_torque, container, false);
        text_meter = (Spinner) view.findViewById(R.id.spinner2);
        button_meter = (Button)view.findViewById(R.id.button);
        mEdit = (EditText)view.findViewById(R.id.Text_value);
        result1 = (TextView) view.findViewById(R.id.Text_value1);
        result2 = (TextView) view.findViewById(R.id.Text_value2);
        result3 = (TextView) view.findViewById(R.id.Text_value3);
        result4 = (TextView) view.findViewById(R.id.Text_value4);
        result5 = (TextView)view.findViewById(R.id.Text_value5);
        result6 = (TextView)view.findViewById(R.id.Text_value6);
        result7 = (TextView)view.findViewById(R.id.Text_value7);

        spinner2 = (Spinner) view.findViewById(R.id.spinner2);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.Torque, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);

        Typeface Lato = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Light.ttf");
        Typeface LatoMedium = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Medium.ttf");
        Typeface LatoRegular = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Regular.ttf");

        TextView tv1 = (TextView)view.findViewById(R.id.textView1);
        TextView tv2 = (TextView)view.findViewById(R.id.textView2);
        TextView tv3 = (TextView)view.findViewById(R.id.textView3);
        TextView tv4 = (TextView)view.findViewById(R.id.textView4);
        TextView tv5 = (TextView)view.findViewById(R.id.textView5);
        TextView tv6 = (TextView)view.findViewById(R.id.textView6);
        TextView tv7 = (TextView)view.findViewById(R.id.textView7);


        TextView tv21 = (TextView)view.findViewById(R.id.Text_value1);
        TextView tv22 = (TextView)view.findViewById(R.id.Text_value2);
        TextView tv23 = (TextView)view.findViewById(R.id.Text_value3);
        TextView tv24 = (TextView)view.findViewById(R.id.Text_value4);
        TextView tv25 = (TextView)view.findViewById(R.id.Text_value5);
        TextView tv26 = (TextView)view.findViewById(R.id.Text_value6);
        TextView tv27 = (TextView)view.findViewById(R.id.Text_value7);
        ;

        TextView tv14 = (TextView)view.findViewById(R.id.title);
        Button bt14 =(Button)view.findViewById(R.id.button);

        Spinner sp1 = (Spinner)view.findViewById(R.id.spinner2);


        tv1.setTypeface(Lato);
        tv2.setTypeface(Lato);
        tv3.setTypeface(Lato);
        tv4.setTypeface(Lato);
        tv5.setTypeface(Lato);
        tv6.setTypeface(Lato);
        tv7.setTypeface(Lato);


        tv21.setTypeface(Lato);
        tv22.setTypeface(Lato);
        tv23.setTypeface(Lato);
        tv24.setTypeface(Lato);
        tv25.setTypeface(Lato);
        tv26.setTypeface(Lato);
        tv27.setTypeface(Lato);

        tv14.setTypeface(LatoMedium);
        bt14.setTypeface(LatoRegular);

        button_meter.setOnClickListener(
                new View.OnClickListener() {

                    public void onClick(View view) {

                        Float divisor = Float.valueOf(conversionObject.get( spinner2.getSelectedItem().toString() ).getAsString() );

                        Float val = Float.valueOf( mEdit.getText().toString() ) / divisor ;
                        double value1 =val * 1;
                        result1.setText(String.valueOf( val * 100) );
                        result2.setText(String.valueOf( val * 1000 ));
                        result3.setText(String.valueOf( val * 0.001 ));
                        result4.setText(String.valueOf( val * 0.001 ));
                        result5.setText(String.valueOf( val * 100000 ));
                        result6.setText(String.valueOf( val * 0.7375621492 ));
                        result7.setText(String.valueOf( val * 8.8507457898 ));


                    }
                });






    return view;
    }


}
