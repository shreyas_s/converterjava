package com.example.android.converter;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * A simple {@link Fragment} subclass.
 */
public class FlowRateFragment extends Fragment {


    Spinner text_meter, spinner2;
    Button button_meter;
    EditText mEdit;
    double valueTo;
    TextView result1, result2, result3, result4, result5,result6,result7, result8, result9, result10, result11, result12, result13, result14, result15, result16, result17, result18, result19, result20, result21, result22, result23, result24;
    JsonParser jp = new JsonParser();
    String converstionsString = "{\"Cubic meter/sec\":\"0.0166666667\", \"Cubic meter/min\":\"1\", \"Cubic meter/hour\":\"60\", \"Liter/sec\":\"16.6666666666\", \"Liter/min\":\"1000\", \"Liter/hour\":\"60000\", \"Cubicfoot/sec\":\"0.5885777787\", \"Cubicfoot/min\":\"35.3146667197\", \"Cubicfoot/hour\":\"2118.8800031838\", \"Cubicinch/sec\":\"1017.062401528\", \"Cubicinch/min\":\"61023.7440916779\", \"Cubicinch/hour\":\"3661424.64549467\",\"US gallon/sec\":\"4.4028675393\", \"US gallon/min\":\"264.1720523575\", \"US gallon/hour\":\"15850.3231414482\", \"UK gallon/sec\":\"3.666153911\", \"UK gallon/min\":\"219.9692346596\", \"UK gallon/hour\":\"13198.1540795736\", \"US oz/sec\":\"563.5670450299\", \"US oz/min\":\"33814.0227017923\", \"US oz/hour\":\"2028841.36210394\", \"UK oz/sec\":\"586.5846257592\", \"UK oz/min\":\"35195.0775455296\", \"UK oz/hour\":\"2111704.65273177\"}";
    JsonObject conversionObject = (JsonObject) jp.parse( converstionsString );
    public FlowRateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        // Inflate the layout for this fragment
        final View root = inflater.inflate(R.layout.fragment_flow_rate, container, false);
        text_meter = (Spinner) root.findViewById(R.id.spinner2);
        button_meter = (Button)root.findViewById(R.id.button);
        mEdit = (EditText)root.findViewById(R.id.Text_value);
        result1 = (TextView) root.findViewById(R.id.Text_value1);
        result2 = (TextView) root.findViewById(R.id.Text_value2);
        result3 = (TextView) root.findViewById(R.id.Text_value3);
        result4 = (TextView) root.findViewById(R.id.Text_value4);
        result5 = (TextView)root.findViewById(R.id.Text_value5);
        result6 = (TextView)root.findViewById(R.id.Text_value6);
        result7 = (TextView)root.findViewById(R.id.Text_value7);
        result8 = (TextView)root.findViewById(R.id.Text_value8);
        result9 = (TextView)root.findViewById(R.id.Text_value9);
        result10 = (TextView)root.findViewById(R.id.Text_value10);
        result11 = (TextView)root.findViewById(R.id.Text_value11);
        result12 = (TextView)root.findViewById(R.id.Text_value12);
        result13 = (TextView) root.findViewById(R.id.Text_value13);
        result14 = (TextView) root.findViewById(R.id.Text_value14);
        result15 = (TextView) root.findViewById(R.id.Text_value15);
        result16 = (TextView) root.findViewById(R.id.Text_value16);
        result17 = (TextView)root.findViewById(R.id.Text_value17);
        result18 = (TextView)root.findViewById(R.id.Text_value18);
        result19 = (TextView)root.findViewById(R.id.Text_value19);
        result20 = (TextView)root.findViewById(R.id.Text_value20);
        result21 = (TextView)root.findViewById(R.id.Text_value21);
        result22 = (TextView)root.findViewById(R.id.Text_value22);
        result23 = (TextView)root.findViewById(R.id.Text_value23);
        result24 = (TextView)root.findViewById(R.id.Text_value24);
        spinner2 = (Spinner) root.findViewById(R.id.spinner2);


        Typeface Lato = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Light.ttf");
        Typeface LatoMedium = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Medium.ttf");
        Typeface LatoRegular = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Regular.ttf");

        TextView tv1 = (TextView)root.findViewById(R.id.textView1);
        TextView tv2 = (TextView)root.findViewById(R.id.textView2);
        TextView tv3 = (TextView)root.findViewById(R.id.textView3);
        TextView tv4 = (TextView)root.findViewById(R.id.textView4);
        TextView tv5 = (TextView)root.findViewById(R.id.textView5);
        TextView tv6 = (TextView)root.findViewById(R.id.textView6);
        TextView tv7 = (TextView)root.findViewById(R.id.textView7);
        TextView tv8 = (TextView)root.findViewById(R.id.textView8);
        TextView tv9 = (TextView)root.findViewById(R.id.textView9);
        TextView tv10 = (TextView)root.findViewById(R.id.textView10);
        TextView tv11 = (TextView)root.findViewById(R.id.textView11);
        TextView tv12 = (TextView)root.findViewById(R.id.textView12);
        TextView tv21 = (TextView)root.findViewById(R.id.textView13);
        TextView tv22 = (TextView)root.findViewById(R.id.textView14);
        TextView tv23 = (TextView)root.findViewById(R.id.textView15);
        TextView tv24 = (TextView)root.findViewById(R.id.textView16);
        TextView tv25 = (TextView)root.findViewById(R.id.textView17);
        TextView tv26 = (TextView)root.findViewById(R.id.textView18);
        TextView tv27 = (TextView)root.findViewById(R.id.textView19);
        TextView tv28 = (TextView)root.findViewById(R.id.textView20);
        TextView tv29 = (TextView)root.findViewById(R.id.textView21);
        TextView tv30 = (TextView)root.findViewById(R.id.textView22);
        TextView tv31 = (TextView)root.findViewById(R.id.textView23);
        TextView tv32 = (TextView)root.findViewById(R.id.textView24);

        TextView tv34 = (TextView)root.findViewById(R.id.Text_value1);
        TextView tv35 = (TextView)root.findViewById(R.id.Text_value2);
        TextView tv36 = (TextView)root.findViewById(R.id.Text_value3);
        TextView tv37 = (TextView)root.findViewById(R.id.Text_value4);
        TextView tv38 = (TextView)root.findViewById(R.id.Text_value5);
        TextView tv39 = (TextView)root.findViewById(R.id.Text_value6);
        TextView tv40 = (TextView)root.findViewById(R.id.Text_value7);
        TextView tv41 = (TextView)root.findViewById(R.id.Text_value8);
        TextView tv42 = (TextView)root.findViewById(R.id.Text_value9);
        TextView tv43 = (TextView)root.findViewById(R.id.Text_value10);
        TextView tv44 = (TextView)root.findViewById(R.id.Text_value11);
        TextView tv45 = (TextView)root.findViewById(R.id.Text_value12);
        TextView tv46 = (TextView)root.findViewById(R.id.Text_value13);
        TextView tv47 = (TextView)root.findViewById(R.id.Text_value14);
        TextView tv48 = (TextView)root.findViewById(R.id.Text_value15);
        TextView tv49 = (TextView)root.findViewById(R.id.Text_value16);
        TextView tv50 = (TextView)root.findViewById(R.id.Text_value17);
        TextView tv51 = (TextView)root.findViewById(R.id.Text_value18);
        TextView tv52 = (TextView)root.findViewById(R.id.Text_value19);
        TextView tv53 = (TextView)root.findViewById(R.id.Text_value20);
        TextView tv54 = (TextView)root.findViewById(R.id.Text_value21);
        TextView tv55 = (TextView)root.findViewById(R.id.Text_value22);
        TextView tv56 = (TextView)root.findViewById(R.id.Text_value23);
        TextView tv57 = (TextView)root.findViewById(R.id.Text_value24);







        TextView tv13 = (TextView)root.findViewById(R.id.title);
        Button bt14 =(Button)root.findViewById(R.id.button);


        tv1.setTypeface(Lato);
        tv2.setTypeface(Lato);
        tv3.setTypeface(Lato);
        tv4.setTypeface(Lato);
        tv5.setTypeface(Lato);
        tv6.setTypeface(Lato);
        tv7.setTypeface(Lato);
        tv8.setTypeface(Lato);
        tv9.setTypeface(Lato);
        tv10.setTypeface(Lato);
        tv11.setTypeface(Lato);
        tv12.setTypeface(Lato);
        tv21.setTypeface(Lato);
        tv22.setTypeface(Lato);
        tv23.setTypeface(Lato);
        tv24.setTypeface(Lato);
        tv25.setTypeface(Lato);
        tv26.setTypeface(Lato);
        tv27.setTypeface(Lato);
        tv28.setTypeface(Lato);
        tv29.setTypeface(Lato);
        tv30.setTypeface(Lato);
        tv31.setTypeface(Lato);
        tv32.setTypeface(Lato);
        tv34.setTypeface(Lato);
        tv35.setTypeface(Lato);
        tv36.setTypeface(Lato);
        tv37.setTypeface(Lato);
        tv38.setTypeface(Lato);
        tv39.setTypeface(Lato);
        tv40.setTypeface(Lato);
        tv41.setTypeface(Lato);
        tv42.setTypeface(Lato);
        tv43.setTypeface(Lato);
        tv44.setTypeface(Lato);
        tv45.setTypeface(Lato);
        tv46.setTypeface(Lato);
        tv47.setTypeface(Lato);
        tv48.setTypeface(Lato);
        tv49.setTypeface(Lato);
        tv50.setTypeface(Lato);
        tv51.setTypeface(Lato);
        tv52.setTypeface(Lato);
        tv53.setTypeface(Lato);
        tv54.setTypeface(Lato);
        tv55.setTypeface(Lato);
        tv56.setTypeface(Lato);
        tv57.setTypeface(Lato);
        tv34.setTypeface(Lato);
        tv34.setTypeface(Lato);
        tv34.setTypeface(Lato);
        tv34.setTypeface(Lato);


        tv13.setTypeface(LatoMedium);
        bt14.setTypeface(LatoRegular);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.Flowrate, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);
        button_meter.setOnClickListener(
                new View.OnClickListener() {

                    public void onClick(View view) {

                        Float divisor = Float.valueOf(conversionObject.get( spinner2.getSelectedItem().toString() ).getAsString() );

                        Float val = Float.valueOf( mEdit.getText().toString() ) / divisor ;
                        valueTo = val *0.0166666667;
                        result1.setText(String.valueOf( val * 1));
                        result2.setText(String.valueOf( val * 0.0166666667));
                        result3.setText(String.valueOf( val * 60 ));
                        result4.setText(String.valueOf( val * 1000 ));
                        result5.setText(String.valueOf( val * 16.6666666666));
                        result6.setText(String.valueOf( val * 60000 ));
                        result7.setText(String.valueOf( val * 35.3146667197));
                        result8.setText(String.valueOf( val * 0.5885777787));
                        result9.setText(String.valueOf( val * 2118.8800031838 ));
                        result10.setText(String.valueOf( val * 61023.7440916779));
                        result11.setText(String.valueOf( val * 1017.062401528));
                        result12.setText(String.valueOf( val * 3661424.64549467 ));
                        result13.setText(String.valueOf( val * 264.1720523575));
                        result14.setText(String.valueOf( val * 4.4028675393));
                        result15.setText(String.valueOf( val * 15850.3231414482 ));
                        result16.setText(String.valueOf( val * 219.9692346596));
                        result17.setText(String.valueOf( val * 3.666153911));
                        result18.setText(String.valueOf( val * 13198.1540795736 ));
                        result19.setText(String.valueOf( val * 33814.0227017923));
                        result20.setText(String.valueOf( val * 563.5670450299));
                        result21.setText(String.valueOf( val * 2028841.36210394 ));
                        result22.setText(String.valueOf( val * 35195.0775455296));
                        result23.setText(String.valueOf( val * 586.5846257592));
                        result24.setText(String.valueOf( val * 2111704.65273177 ));

                    }
                });

    return root;
    }


}
