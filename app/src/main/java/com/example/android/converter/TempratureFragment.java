package com.example.android.converter;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.math.BigInteger;


/**
 * A simple {@link Fragment} subclass.
 */
public class TempratureFragment extends Fragment {
    Spinner text_meter, spinner2;
    Button button_meter;
    EditText mEdit;
    TextView result1, result2, result3, result4;
    JsonParser jp = new JsonParser();
    String converstionsString = "{\"Celsius\":\"0\", \"Fahrenheit\":\"0\", \"Kelvin\":\"0\", \"Rankine\":\"0\"}";
    JsonObject conversionObject = (JsonObject) jp.parse( converstionsString );
    String FromValue;
    String bi1;
    BigInteger bi2;

    public TempratureFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_temprature, container, false);
        text_meter = (Spinner) view.findViewById(R.id.spinner2);
        button_meter = (Button)view.findViewById(R.id.button);
        mEdit = (EditText)view.findViewById(R.id.Text_value);
        result1 = (TextView) view.findViewById(R.id.Text_value1);
        result2 = (TextView) view.findViewById(R.id.Text_value2);
        result3 = (TextView) view.findViewById(R.id.Text_value3);
        result4 = (TextView) view.findViewById(R.id.Text_value4);
        spinner2 = (Spinner) view.findViewById(R.id.spinner2);

        /*Fonts */
        Typeface Lato = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Light.ttf");
        Typeface LatoMedium = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Medium.ttf");
        Typeface LatoRegular = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Regular.ttf");

        TextView tv1 = (TextView)view.findViewById(R.id.textView1);
        TextView tv2 = (TextView)view.findViewById(R.id.textView2);
        TextView tv3 = (TextView)view.findViewById(R.id.textView3);
        TextView tv4 = (TextView)view.findViewById(R.id.textView4);
        TextView tv21 = (TextView)view.findViewById(R.id.Text_value1);
        TextView tv22 = (TextView)view.findViewById(R.id.Text_value2);
        TextView tv23 = (TextView)view.findViewById(R.id.Text_value3);
        TextView tv24 = (TextView)view.findViewById(R.id.Text_value4);

        tv1.setTypeface(Lato);
        tv2.setTypeface(Lato);
        tv3.setTypeface(Lato);
        tv4.setTypeface(Lato);
        tv21.setTypeface(Lato);
        tv22.setTypeface(Lato);
        tv23.setTypeface(Lato);
        tv24.setTypeface(Lato);
        TextView tv5 = (TextView)view.findViewById(R.id.title);
        Button bt1 =(Button)view.findViewById(R.id.button);
        tv5.setTypeface(LatoMedium);
        bt1.setTypeface(LatoRegular);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.Temp, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);
        button_meter.setOnClickListener(
                new View.OnClickListener() {

                    public void onClick(View view) {



                        Float divisor = Float.valueOf(conversionObject.get(spinner2.getSelectedItem().toString()).getAsString());
                        FromValue = spinner2.getSelectedItem().toString();

                        Float val = Float.valueOf(mEdit.getText().toString());
                        switch (spinner2.getSelectedItem().toString()) {
                            case "Celsius":
                                result1.setText(String.valueOf(mEdit.getText().toString()));
                                result2.setText(String.valueOf((val *1.8)+32));
                                result3.setText(String.valueOf(val + 273.15));
                                result4.setText(String.valueOf((val + 273.15)*1.8));
                        break;
                            case "Fahrenheit":
                                result1.setText(String.valueOf((val - 32)/1.8));
                                result2.setText(String.valueOf(mEdit.getText().toString()));
                                result3.setText(String.valueOf((val+ 459.67)/1.8));
                                result4.setText(String.valueOf(val + 459.67));
                                break;
                            case "Kelvin":
                                result1.setText(String.valueOf(val -273.15));
                                result2.setText(String.valueOf((val-273.15)*1.800 + 32.00));
                                result3.setText(String.valueOf(mEdit.getText().toString()));
                                result4.setText(String.valueOf(val*1.8));
                                break;
                            case "Rankine":
                                result1.setText(String.valueOf((val-491.67) * 1.8));
                                result2.setText(String.valueOf(val -459.67));
                                result3.setText(String.valueOf(val*1.8));
                                result4.setText(String.valueOf(mEdit.getText().toString()));

                        }


                    }
                });




    return view;
    }


}
