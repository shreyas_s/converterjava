package com.example.android.converter;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.math.BigInteger;


/**
 * A simple {@link Fragment} subclass.
 */
public class StorageFragment extends Fragment {
    Spinner text_meter, spinner2;
    Button button_meter;
    EditText mEdit;
    TextView result1, result2, result3, result4, result5,result6,result7, result8, result9, result10, result11,result12,result13,result14,result15;
    BigInteger bi1;

    JsonParser jp = new JsonParser();
    String converstionsString = "{\"bit\":\"0.0001220703125\", \"byte\":\"0.0009765625\", \"exabit\":\"140737488355007.4\", \"exabyte\":\"1125899906846249\", \"gigabit\":\"131072\", \"gigabyte\":\"1048576\", \"kilobit\":\"0.125\", \"kilobyte\":\"1\", \"megabit\":\"128\", \"megabyte\":\"1024\", \"petabit\":\"137438953472.43954\", \"petabyte\":\"1099511627770.6619\", \"terabit\":\"134217728.02199024\", \"terabyte\":\"1073741824.457397\"}";
    JsonObject conversionObject = (JsonObject) jp.parse( converstionsString );


    public StorageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_storage, container, false);
        text_meter = (Spinner) view.findViewById(R.id.spinner2);
        button_meter = (Button)view.findViewById(R.id.button);
        mEdit = (EditText)view.findViewById(R.id.Text_value);
        result1 = (TextView) view.findViewById(R.id.Text_value1);
        result2 = (TextView) view.findViewById(R.id.Text_value2);
        result3 = (TextView) view.findViewById(R.id.Text_value3);
        result4 = (TextView) view.findViewById(R.id.Text_value4);
        result5 = (TextView)view.findViewById(R.id.Text_value5);
        result6 = (TextView)view.findViewById(R.id.Text_value6);
        result7 = (TextView)view.findViewById(R.id.Text_value7);
        result8 = (TextView)view.findViewById(R.id.Text_value8);
        result9 = (TextView)view.findViewById(R.id.Text_value9);
        result10 = (TextView)view.findViewById(R.id.Text_value10);
        result11 = (TextView)view.findViewById(R.id.Text_value11);
        result12 = (TextView)view.findViewById(R.id.Text_value12);
        result13 = (TextView)view.findViewById(R.id.Text_value13);
        result14 = (TextView)view.findViewById(R.id.Text_value14);
        result15 = (TextView)view.findViewById(R.id.Text_value15);


        spinner2 = (Spinner) view.findViewById(R.id.spinner2);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.Storage, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);


        Typeface Lato = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Light.ttf");
        Typeface LatoMedium = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Medium.ttf");
        Typeface LatoRegular = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Regular.ttf");

        TextView tv1 = (TextView)view.findViewById(R.id.textView1);
        TextView tv2 = (TextView)view.findViewById(R.id.textView2);
        TextView tv3 = (TextView)view.findViewById(R.id.textView3);
        TextView tv4 = (TextView)view.findViewById(R.id.textView4);
        TextView tv5 = (TextView)view.findViewById(R.id.textView5);
        TextView tv6 = (TextView)view.findViewById(R.id.textView6);
        TextView tv7 = (TextView)view.findViewById(R.id.textView7);
        TextView tv8 = (TextView)view.findViewById(R.id.textView8);
        TextView tv9 = (TextView)view.findViewById(R.id.textView9);
        TextView tv10 = (TextView)view.findViewById(R.id.textView10);
        TextView tv11 = (TextView)view.findViewById(R.id.textView11);
        TextView tv12 = (TextView)view.findViewById(R.id.textView12);
        TextView tv13 = (TextView)view.findViewById(R.id.textView13);
        TextView tv14 = (TextView)view.findViewById(R.id.textView14);
        TextView tv21 = (TextView)view.findViewById(R.id.Text_value1);
        TextView tv22 = (TextView)view.findViewById(R.id.Text_value2);
        TextView tv23 = (TextView)view.findViewById(R.id.Text_value3);
        TextView tv24 = (TextView)view.findViewById(R.id.Text_value4);
        TextView tv25 = (TextView)view.findViewById(R.id.Text_value5);
        TextView tv26 = (TextView)view.findViewById(R.id.Text_value6);
        TextView tv27 = (TextView)view.findViewById(R.id.Text_value7);
        TextView tv28 = (TextView)view.findViewById(R.id.Text_value8);
        TextView tv29 = (TextView)view.findViewById(R.id.Text_value9);
        TextView tv30 = (TextView)view.findViewById(R.id.Text_value10);
        TextView tv31 = (TextView)view.findViewById(R.id.Text_value11);
        TextView tv32 = (TextView)view.findViewById(R.id.Text_value12);
        TextView tv33 = (TextView)view.findViewById(R.id.Text_value13);
        TextView tv34 = (TextView)view.findViewById(R.id.Text_value14);
                ;
        TextView tv15 = (TextView)view.findViewById(R.id.title);
        Button bt14 =(Button)view.findViewById(R.id.button);

        Spinner sp1 = (Spinner)view.findViewById(R.id.spinner2);


        tv1.setTypeface(Lato);
        tv2.setTypeface(Lato);
        tv3.setTypeface(Lato);
        tv4.setTypeface(Lato);
        tv5.setTypeface(Lato);
        tv6.setTypeface(Lato);
        tv7.setTypeface(Lato);
        tv8.setTypeface(Lato);
        tv9.setTypeface(Lato);
        tv10.setTypeface(Lato);
        tv11.setTypeface(Lato);
        tv12.setTypeface(Lato);
        tv13.setTypeface(Lato);
        tv14.setTypeface(Lato);
        tv21.setTypeface(Lato);
        tv22.setTypeface(Lato);
        tv23.setTypeface(Lato);
        tv24.setTypeface(Lato);
        tv25.setTypeface(Lato);
        tv26.setTypeface(Lato);
        tv27.setTypeface(Lato);
        tv28.setTypeface(Lato);
        tv29.setTypeface(Lato);
        tv30.setTypeface(Lato);
        tv31.setTypeface(Lato);
        tv32.setTypeface(Lato);
        tv33.setTypeface(Lato);
        tv34.setTypeface(Lato);
        tv15.setTypeface(LatoMedium);
        bt14.setTypeface(LatoRegular);



        button_meter.setOnClickListener(
                new View.OnClickListener() {

                    public void onClick(View view) {

                        Float divisor = Float.valueOf(conversionObject.get(spinner2.getSelectedItem().toString()).getAsString());
                        Float val = Float.valueOf(mEdit.getText().toString()) / divisor;
                        Float val1= Float.valueOf(mEdit.getText().toString());

                        double cd = Float.valueOf(val1 * 1024) / 700;
                        double floppy = Float.valueOf(val1 * 1024)/1474.56;
                        double dvd = Float.valueOf(val1 * 1024)/4.58984375;

                        double roundOffcd = Math.round(cd * 100.0) / 100.0;
                        double roundOfffloppy = Math.round(floppy * 100.0) / 100.0;
                        double roundOffdvd = Math.round(dvd * 100.0) / 100.0;


                        switch (spinner2.getSelectedItem().toString()) {
                            case "gigabyte":
                                result15.setText(String.valueOf(roundOffcd) + " CD's");
                                break;
                            case "megabyte":
                                result15.setText(String.valueOf(roundOfffloppy) +" Floppy disk");
                                break;
                            case "terabyte":
                                result15.setText(String.valueOf(roundOffdvd)+" DVDs");
                                break;
                        }

                        result1.setText(String.valueOf(val * 0.0001220703125));
                        result2.setText(String.valueOf(val * 0.0009765625));
                        result3.setText(String.valueOf(val * 140737488355007.4));
                        bi1 = new BigInteger("1125899906846249");
                        result4.setText(String.valueOf(val * bi1.floatValue()));
                        result5.setText(String.valueOf(val * 131072));
                        result6.setText(String.valueOf(val * 1048576));
                        result7.setText(String.valueOf(val * 0.125));
                        result8.setText(String.valueOf(val * 1));
                        result9.setText(String.valueOf(val * 128));
                        result10.setText(String.valueOf(val * 1024));
                        result11.setText(String.valueOf(val * 137438953472.43954));
                        result12.setText(String.valueOf(val * 1099511627770.6619));
                        result13.setText(String.valueOf(val * 134217728.02199024));
                        result14.setText(String.valueOf(val * 1073741824.457397));


                    }
                });





        return view;
    }


}
