package com.example.android.converter;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class CurrencyFragment extends Fragment
{
    Spinner text_meter, spinner2;
    Button button_meter;
    EditText mEdit;
    TextView result1, result2, result3, result4, result5,result6,result7, result8, result9, result10, result11, result12;


    MyRestClient myRestClient;

    public CurrencyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_currency, container, false);
        text_meter = (Spinner) view.findViewById(R.id.spinner2);
        button_meter = (Button)view.findViewById(R.id.button);
        mEdit = (EditText)view.findViewById(R.id.Text_value);
        final SharedPreferences pref = getActivity().getSharedPreferences("asd", Context.MODE_PRIVATE);
        Typeface Lato = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Light.ttf");
        Typeface LatoMedium = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Medium.ttf");
        Typeface LatoRegular = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Regular.ttf");
        result1 = (TextView) view.findViewById(R.id.Text_value1);
        result2 = (TextView) view.findViewById(R.id.Text_value2);
        result3 = (TextView) view.findViewById(R.id.Text_value3);
        result4 = (TextView) view.findViewById(R.id.Text_value4);
        result5 = (TextView)view.findViewById(R.id.Text_value5);
        result6 = (TextView)view.findViewById(R.id.Text_value6);
        result7 = (TextView)view.findViewById(R.id.Text_value7);
        result8 = (TextView)view.findViewById(R.id.Text_value8);
        result9 = (TextView)view.findViewById(R.id.Text_value9);
        result10 = (TextView)view.findViewById(R.id.Text_value10);
        result11 = (TextView)view.findViewById(R.id.Text_value11);
        result12 = (TextView)view.findViewById(R.id.Text_value12);

        TextView tv1 = (TextView)view.findViewById(R.id.textView1);
        TextView tv2 = (TextView)view.findViewById(R.id.textView2);
        TextView tv3 = (TextView)view.findViewById(R.id.textView3);
        TextView tv4 = (TextView)view.findViewById(R.id.textView4);
        TextView tv5 = (TextView)view.findViewById(R.id.textView5);
        TextView tv6 = (TextView)view.findViewById(R.id.textView6);
        TextView tv7 = (TextView)view.findViewById(R.id.textView7);
        TextView tv8 = (TextView)view.findViewById(R.id.textView8);
        TextView tv9 = (TextView)view.findViewById(R.id.textView9);
        TextView tv10 = (TextView)view.findViewById(R.id.textView10);
        TextView tv11 = (TextView)view.findViewById(R.id.textView11);
        TextView tv12 = (TextView)view.findViewById(R.id.textView12);
        TextView tv21 = (TextView)view.findViewById(R.id.Text_value1);
        TextView tv22 = (TextView)view.findViewById(R.id.Text_value2);
        TextView tv23 = (TextView)view.findViewById(R.id.Text_value3);
        TextView tv24 = (TextView)view.findViewById(R.id.Text_value4);
        TextView tv25 = (TextView)view.findViewById(R.id.Text_value5);
        TextView tv26 = (TextView)view.findViewById(R.id.Text_value6);
        TextView tv27 = (TextView)view.findViewById(R.id.Text_value7);
        TextView tv28 = (TextView)view.findViewById(R.id.Text_value8);
        TextView tv29 = (TextView)view.findViewById(R.id.Text_value9);
        TextView tv30 = (TextView)view.findViewById(R.id.Text_value10);
        TextView tv31 = (TextView)view.findViewById(R.id.Text_value11);
        TextView tv32 = (TextView)view.findViewById(R.id.Text_value12);
        TextView tv13 = (TextView)view.findViewById(R.id.title);
        Button bt14 =(Button)view.findViewById(R.id.button);


        Spinner sp1 = (Spinner)view.findViewById(R.id.spinner2);


        tv1.setTypeface(Lato);
        tv2.setTypeface(Lato);
        tv3.setTypeface(Lato);
        tv4.setTypeface(Lato);
        tv5.setTypeface(Lato);
        tv6.setTypeface(Lato);
        tv7.setTypeface(Lato);
        tv8.setTypeface(Lato);
        tv9.setTypeface(Lato);
        tv10.setTypeface(Lato);
        tv11.setTypeface(Lato);
        tv12.setTypeface(Lato);
        tv21.setTypeface(Lato);
        tv22.setTypeface(Lato);
        tv23.setTypeface(Lato);
        tv24.setTypeface(Lato);
        tv25.setTypeface(Lato);
        tv26.setTypeface(Lato);
        tv27.setTypeface(Lato);
        tv28.setTypeface(Lato);
        tv29.setTypeface(Lato);
        tv30.setTypeface(Lato);
        tv31.setTypeface(Lato);
        tv32.setTypeface(Lato);

        tv13.setTypeface(LatoMedium);
        bt14.setTypeface(LatoRegular);
        spinner2 = (Spinner) view.findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.Currency, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);

        myRestClient.getClient().getCurrencies(new Callback<JsonObject>() {
            @Override
            public void success(JsonObject jsonObject, Response response) {
                JsonPrimitive timeStampDate = jsonObject.getAsJsonObject().getAsJsonPrimitive("timestamp");
                final JsonObject quotes = jsonObject.getAsJsonObject("quotes");
                //final JsonObject date = jsonObject.date("timestamp");
                final JsonParser jp = new JsonParser();
                final JsonObject conversionObject = (JsonObject) jp.parse(String.valueOf(quotes));

                //saving data
                SharedPreferences.Editor prefsEditor = pref.edit();
                Gson gson = new Gson();
                String json = gson.toJson(quotes);
                prefsEditor.putString("quotes", json);
                prefsEditor.commit();


                //Getting data
                Gson gson1 = new Gson();
                String json1 = pref.getString("quotes", null);
                JsonObject inMemoryRates = (JsonObject) jp.parse(String.valueOf(json1));

                inMemoryRates.get("USD2EUR");

               // quotes obj = gson.fromJson(json, quotes.class);
                Log.d("LOL",json1);

                final Float usd2inr1 = Float.valueOf(inMemoryRates.get("USDEUR").toString());
                final Float usd2inr = Float.valueOf(quotes.get("USDINR").toString());
                //Float datetime = Float.valueOf(timeStampDate.get(("timeStampDate")).toString());
                final Float usd2eur = Float.valueOf(quotes.get("USDEUR").toString());
                Toast.makeText(getActivity(), "USD to EURO rate is " + usd2inr1, Toast.LENGTH_SHORT).show();
                button_meter.setOnClickListener(
                        new View.OnClickListener() {

                            public void onClick(View view) {

                                Float divisor = Float.valueOf(conversionObject.get(spinner2.getSelectedItem().toString()).getAsString());

                                Float val = Float.valueOf(mEdit.getText().toString()) / divisor;

                                if (spinner2.getSelectedItem().toString() == "USDINR") {
                                    Toast.makeText(getActivity(), "USD to EURO rate is " + usd2eur, Toast.LENGTH_SHORT).show();
                                }

                                result1.setText(String.valueOf(val * Float.valueOf(quotes.get("USDEUR").toString())));
                                result2.setText(String.valueOf(val * Float.valueOf(quotes.get("USDGBP").toString())));
                                result3.setText(String.valueOf(val * Float.valueOf(quotes.get("USDCAD").toString())));
                                result4.setText(String.valueOf(val * Float.valueOf(quotes.get("USDPLN").toString())));
                                result5.setText(String.valueOf(val * Float.valueOf(quotes.get("USDUSD").toString())));
                                result6.setText(String.valueOf(val * Float.valueOf(quotes.get("USDINR").toString())));
                                result7.setText(String.valueOf(val * Float.valueOf(quotes.get("USDMXN").toString())));
                                result8.setText(String.valueOf(val * Float.valueOf(quotes.get("USDAUD").toString())));
                                result9.setText(String.valueOf(val * Float.valueOf(quotes.get("USDBGN").toString())));
                                result10.setText(String.valueOf(val * Float.valueOf(quotes.get("USDCZK").toString())));
                                result11.setText(String.valueOf(val * Float.valueOf(quotes.get("USDCNY").toString())));
                                result12.setText(String.valueOf(val * Float.valueOf(quotes.get("USDDKK").toString())));

                            }
                        });

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity(), "Could not fetch Live currencies ", Toast.LENGTH_SHORT).show();
                final JsonParser jp = new JsonParser();
                Gson gson1 = new Gson();
                final String json1 = pref.getString("quotes", null);
                final JsonObject inMemoryRates = (JsonObject) jp.parse(String.valueOf(json1));

                final Float usd2inr1 = Float.valueOf(inMemoryRates.get("USDINR").toString());
                //Toast.makeText(getActivity(), "USD to EURO rate is " + usd2inr1, Toast.LENGTH_SHORT).show();

                button_meter.setOnClickListener(
                        new View.OnClickListener() {

                            public void onClick(View view) {

                                Float divisor = Float.valueOf(inMemoryRates.get(spinner2.getSelectedItem().toString()).getAsString());

                                Float val = Float.valueOf(mEdit.getText().toString()) / divisor;


                                result1.setText(String.valueOf(val * Float.valueOf(inMemoryRates.get("USDINR").toString())));
                                result2.setText(String.valueOf(val * Float.valueOf(inMemoryRates.get("USDGBP").toString())));
                                result3.setText(String.valueOf(val * Float.valueOf(inMemoryRates.get("USDCAD").toString())));
                                result4.setText(String.valueOf(val * Float.valueOf(inMemoryRates.get("USDPLN").toString())));
                                result5.setText(String.valueOf(val * Float.valueOf(inMemoryRates.get("USDUSD").toString())));
                                result6.setText(String.valueOf(val * Float.valueOf(inMemoryRates.get("USDINR").toString())));
                                result7.setText(String.valueOf(val * Float.valueOf(inMemoryRates.get("USDMXN").toString())));
                                result8.setText(String.valueOf(val * Float.valueOf(inMemoryRates.get("USDAUD").toString())));
                                result9.setText(String.valueOf(val * Float.valueOf(inMemoryRates.get("USDBGN").toString())));
                                result10.setText(String.valueOf(val * Float.valueOf(inMemoryRates.get("USDCZK").toString())));
                                result11.setText(String.valueOf(val * Float.valueOf(inMemoryRates.get("USDCNY").toString())));
                                result12.setText(String.valueOf(val * Float.valueOf(inMemoryRates.get("USDDKK").toString())));

                            }
                        });



            }
        });

        return view;
    }


}
