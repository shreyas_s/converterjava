package com.example.android.converter;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * A simple {@link Fragment} subclass.
 */
public class EnergyFragment extends Fragment {
    Spinner text_meter, spinner2;
    Button button_meter;
    EditText mEdit;
    TextView result1, result2, result3, result4, result5,result6,result7, result8, result9, result10, result11;

    JsonParser jp = new JsonParser();
    String converstionsString = "{\"Watt/hour\":\"1\", \"Watt/sec\":\"3600\", \"Kilowatt/hour\":\"0.001\", \"Kilowatt/sec\":\"3.6\", \"Megawatt/hour\":\"0.000001\", \"Gigawatt/hour\":\"0.000000001\", \"Calorie\":\"859.8452399931\", \"Kilocalorie\":\"0.85984524\", \"Joules\":\"3600\", \"Kilojoules\":\"3.6\", \"BTU\":\"3.412141632\"}";
    JsonObject conversionObject = (JsonObject) jp.parse( converstionsString );


    public EnergyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View root = inflater.inflate(R.layout.fragment_energy, container, false);
        text_meter = (Spinner) root.findViewById(R.id.spinner2);
        button_meter = (Button)root.findViewById(R.id.button);
        mEdit = (EditText)root.findViewById(R.id.Text_value);
        result1 = (TextView) root.findViewById(R.id.Text_value1);
        result2 = (TextView) root.findViewById(R.id.Text_value2);
        result3 = (TextView) root.findViewById(R.id.Text_value3);
        result4 = (TextView) root.findViewById(R.id.Text_value4);
        result5 = (TextView)root.findViewById(R.id.Text_value5);
        result6 = (TextView)root.findViewById(R.id.Text_value6);
        result7 = (TextView)root.findViewById(R.id.Text_value7);
        result8 = (TextView)root.findViewById(R.id.Text_value8);
        result9 = (TextView)root.findViewById(R.id.Text_value9);
        result10 = (TextView)root.findViewById(R.id.Text_value10);
        result11 = (TextView)root.findViewById(R.id.Text_value11);

        /* This changes the font for TextView */
        spinner2 = (Spinner) root.findViewById(R.id.spinner2);


        /*This is Font area */
        Typeface Lato = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Light.ttf");
        Typeface LatoMedium = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Medium.ttf");
        Typeface LatoRegular = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Regular.ttf");

        TextView tv1 = (TextView)root.findViewById(R.id.textView1);
        TextView tv2 = (TextView)root.findViewById(R.id.textView2);
        TextView tv3 = (TextView)root.findViewById(R.id.textView3);
        TextView tv4 = (TextView)root.findViewById(R.id.textView4);
        TextView tv5 = (TextView)root.findViewById(R.id.textView5);
        TextView tv6 = (TextView)root.findViewById(R.id.textView6);
        TextView tv7 = (TextView)root.findViewById(R.id.textView7);
        TextView tv8 = (TextView)root.findViewById(R.id.textView8);
        TextView tv9 = (TextView)root.findViewById(R.id.textView9);
        TextView tv10 = (TextView)root.findViewById(R.id.textView10);
        TextView tv11 = (TextView)root.findViewById(R.id.textView11);
        TextView tv12 = (TextView)root.findViewById(R.id.textView12);
        TextView tv21 = (TextView)root.findViewById(R.id.Text_value1);
        TextView tv22 = (TextView)root.findViewById(R.id.Text_value2);
        TextView tv23 = (TextView)root.findViewById(R.id.Text_value3);
        TextView tv24 = (TextView)root.findViewById(R.id.Text_value4);
        TextView tv25 = (TextView)root.findViewById(R.id.Text_value5);
        TextView tv26 = (TextView)root.findViewById(R.id.Text_value6);
        TextView tv27 = (TextView)root.findViewById(R.id.Text_value7);
        TextView tv28 = (TextView)root.findViewById(R.id.Text_value8);
        TextView tv29 = (TextView)root.findViewById(R.id.Text_value9);
        TextView tv30 = (TextView)root.findViewById(R.id.Text_value10);
        TextView tv31 = (TextView)root.findViewById(R.id.Text_value11);

        TextView tv13 = (TextView)root.findViewById(R.id.title);
        Button bt14 =(Button)root.findViewById(R.id.button);


        Spinner sp1 = (Spinner)root.findViewById(R.id.spinner2);


        tv1.setTypeface(Lato);
        tv2.setTypeface(Lato);
        tv3.setTypeface(Lato);
        tv4.setTypeface(Lato);
        tv5.setTypeface(Lato);
        tv6.setTypeface(Lato);
        tv7.setTypeface(Lato);
        tv8.setTypeface(Lato);
        tv9.setTypeface(Lato);
        tv10.setTypeface(Lato);
        tv11.setTypeface(Lato);

        tv21.setTypeface(Lato);
        tv22.setTypeface(Lato);
        tv23.setTypeface(Lato);
        tv24.setTypeface(Lato);
        tv25.setTypeface(Lato);
        tv26.setTypeface(Lato);
        tv27.setTypeface(Lato);
        tv28.setTypeface(Lato);
        tv29.setTypeface(Lato);
        tv30.setTypeface(Lato);
        tv31.setTypeface(Lato);

        tv13.setTypeface(LatoMedium);
        bt14.setTypeface(LatoRegular);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.Energy, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);

        button_meter.setOnClickListener(
                new View.OnClickListener() {

                    public void onClick(View view) {

                        Float divisor = Float.valueOf(conversionObject.get(spinner2.getSelectedItem().toString()).getAsString());

                        Float val = Float.valueOf(mEdit.getText().toString()) / divisor;

                        result1.setText(String.valueOf(val * 1));
                        result2.setText(String.valueOf(val * 3600));
                        result3.setText(String.valueOf(val * 0.001));
                        result4.setText(String.valueOf(val * 3.6));
                        result5.setText(String.valueOf(val * 0.000001));
                        result6.setText(String.valueOf(val * 0.000000001));
                        result7.setText(String.valueOf(val * 859.8452399931));
                        result8.setText(String.valueOf(val * 0.85984524));
                        result9.setText(String.valueOf(val * 3600));
                        result10.setText(String.valueOf(val * 3.6));
                        result11.setText(String.valueOf(val * 3.412141632));


                    }
                });
        return root;
    }


}
