package com.example.android.converter;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.math.BigInteger;


/**
 * A simple {@link Fragment} subclass.
 */
public class AreaFragment extends Fragment {
    Spinner text_meter, spinner2;
    Button button_meter;
    EditText mEdit;
    TextView result1, result2, result3, result4, result5,result6,result7, result8, result9, result10, result11;
    BigInteger bi1, bi2;

    JsonParser jp = new JsonParser();
    String converstionsString = "{\"Square Km\":\"1\", \"Square m\":\"1000000\", \"Square cm\":\"10000000000\", \"Square mm\":\"1000000000000\", \"Square miles\":\"0.3861006097\", \"Square Yards\":\"1195990.0463\", \"Square Feet\":\"10763910.417\", \"Square inch\":\"1550003100\", \"Acre\":\"247.105381\", \"Cent\":\"24710.5407\", \"Hectare\":\"100\"}";
    JsonObject conversionObject = (JsonObject) jp.parse( converstionsString );
    int recurso;
    Typeface tf;


    public AreaFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {





        // Inflate the layout for this fragment
        final View view =inflater.inflate(R.layout.fragment_area, container, false);
        text_meter = (Spinner) view.findViewById(R.id.spinner2);
        button_meter = (Button)view.findViewById(R.id.button);
        mEdit = (EditText)view.findViewById(R.id.Text_value);
        result1 = (TextView) view.findViewById(R.id.Text_value1);
        result2 = (TextView) view.findViewById(R.id.Text_value2);
        result3 = (TextView) view.findViewById(R.id.Text_value3);
        result4 = (TextView) view.findViewById(R.id.Text_value4);
        result5 = (TextView)view.findViewById(R.id.Text_value5);
        result6 = (TextView)view.findViewById(R.id.Text_value6);
        result7 = (TextView)view.findViewById(R.id.Text_value7);
        result8 = (TextView)view.findViewById(R.id.Text_value8);
        result9 = (TextView)view.findViewById(R.id.Text_value9);
        result10 = (TextView)view.findViewById(R.id.Text_value10);
        result11 = (TextView)view.findViewById(R.id.Text_value11);

        spinner2 = (Spinner) view.findViewById(R.id.spinner2);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.Area, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);


        Typeface Lato = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Light.ttf");
        Typeface LatoMedium = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Medium.ttf");
        Typeface LatoRegular = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/Lato-Regular.ttf");

        TextView tv1 = (TextView)view.findViewById(R.id.textView1);
        TextView tv2 = (TextView)view.findViewById(R.id.textView2);
        TextView tv3 = (TextView)view.findViewById(R.id.textView3);
        TextView tv4 = (TextView)view.findViewById(R.id.textView4);
        TextView tv5 = (TextView)view.findViewById(R.id.textView5);
        TextView tv6 = (TextView)view.findViewById(R.id.textView6);
        TextView tv7 = (TextView)view.findViewById(R.id.textView7);
        TextView tv8 = (TextView)view.findViewById(R.id.textView8);
        TextView tv9 = (TextView)view.findViewById(R.id.textView9);
        TextView tv10 = (TextView)view.findViewById(R.id.textView10);
        TextView tv11 = (TextView)view.findViewById(R.id.textView11);
        TextView tv21 = (TextView)view.findViewById(R.id.Text_value1);
        TextView tv22 = (TextView)view.findViewById(R.id.Text_value2);
        TextView tv23 = (TextView)view.findViewById(R.id.Text_value3);
        TextView tv24 = (TextView)view.findViewById(R.id.Text_value4);
        TextView tv25 = (TextView)view.findViewById(R.id.Text_value5);
        TextView tv26 = (TextView)view.findViewById(R.id.Text_value6);
        TextView tv27 = (TextView)view.findViewById(R.id.Text_value7);
        TextView tv28 = (TextView)view.findViewById(R.id.Text_value8);
        TextView tv29 = (TextView)view.findViewById(R.id.Text_value9);
        TextView tv30 = (TextView)view.findViewById(R.id.Text_value10);
        TextView tv31 = (TextView)view.findViewById(R.id.Text_value11);
        TextView tv13 = (TextView)view.findViewById(R.id.title);
        Button bt14 =(Button)view.findViewById(R.id.button);

        Spinner sp1 = (Spinner)view.findViewById(R.id.spinner2);


        tv1.setTypeface(Lato);
        tv2.setTypeface(Lato);
        tv3.setTypeface(Lato);
        tv4.setTypeface(Lato);
        tv5.setTypeface(Lato);
        tv6.setTypeface(Lato);
        tv7.setTypeface(Lato);
        tv8.setTypeface(Lato);
        tv9.setTypeface(Lato);
        tv10.setTypeface(Lato);
        tv11.setTypeface(Lato);
        tv21.setTypeface(Lato);
        tv22.setTypeface(Lato);
        tv23.setTypeface(Lato);
        tv24.setTypeface(Lato);
        tv25.setTypeface(Lato);
        tv26.setTypeface(Lato);
        tv27.setTypeface(Lato);
        tv28.setTypeface(Lato);
        tv29.setTypeface(Lato);
        tv30.setTypeface(Lato);
        tv31.setTypeface(Lato);
        tv13.setTypeface(LatoMedium);
        bt14.setTypeface(LatoRegular);



        button_meter.setOnClickListener(
                new View.OnClickListener() {

                    public void onClick(View view) {

                        Float divisor = Float.valueOf(conversionObject.get( spinner2.getSelectedItem().toString() ).getAsString() );

                        Float val = Float.valueOf( mEdit.getText().toString() ) / divisor ;

                        result1.setText(String.valueOf( val * 1 ));
                        result2.setText(String.valueOf( val * 1000000 ));
                        bi1 = new BigInteger("10000000000");
                        result3.setText(String.valueOf( val * bi1.floatValue() ));
                        bi2 = new BigInteger("1000000000000");
                        result4.setText(String.valueOf( val * bi2.floatValue() ));
                        result5.setText(String.valueOf( val * 0.3861006097 ));
                        result6.setText(String.valueOf( val * 1195990.0463 ));
                        result7.setText(String.valueOf( val * 10763910.417 ));
                        result8.setText(String.valueOf( val * 1550003100 ));
                        result9.setText(String.valueOf( val * 247.105381 ));
                        result10.setText(String.valueOf( val * 24710.5407 ));
                        result11.setText(String.valueOf( val * 100 ));


                    }
                });

    return view;
    }


}
